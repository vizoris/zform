
$(function() {

// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown').hover(function() {
    $(this).children('.dropdown-menu').stop(true,true).fadeToggle();
     $(this).toggleClass('active')
   })
}else {
  $('.dropdown').click(function() {
    $(this).children('.dropdown-menu').stop(true,true).fadeToggle();
   })
}



 // Банер

 $('.banner').slick({
  dots: true,
  arrows: false,
  speed: 400,
  slidesToShow: 1,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        adaptiveHeight: true
      }
    }
  ]
});




// FansyBox
 $('.fancybox').fancybox({});





// Сллайдер сертификатов
$('.brands-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 7,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 5,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 2
      },
      
    },
    {
      breakpoint: 479,
    settings: {
      slidesToShow: 2
      }
    },
  ]
});






// Меню в сайдбаре
$('.sidebar-menu >li >a').click(function() {
  $(this).toggleClass('.active');
  $(this).next('ul').fadeToggle();
})




// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})












})